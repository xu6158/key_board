#ifndef __KEY_BOARD_SAMPLE_H
#define __KEY_BOARD_SAMPLE_H

#include "stm32f1xx_hal.h"
#include "key_board/key_board.h"

//如果为矩阵键盘则使能此宏
#define USER_KEY_BOARD_MATRIX 0

/*下拉输入，高有效*/
#define KEY_PORT_J12            GPIOA
#define KEY_PIN_J12             GPIO_PIN_0
#define KEY_PRESS_LEVEL_J12     GPIO_PIN_SET
#define KEY_RELEASE_LEVEL_J12   GPIO_PIN_RESET
#define KEY_PORT_J34            GPIOC
#define KEY_PIN_J34             GPIO_PIN_13
#define KEY_PRESS_LEVEL_J34     GPIO_PIN_SET
#define KEY_RELEASE_LEVEL_J34   GPIO_PIN_RESET

#if (USER_KEY_BOARD_MATRIX)
/*推挽输出，低使能，高失能*/
#define KEY_PORT_J135           GPIOE
#define KEY_PIN_J135            GPIO_PIN_5
#define KEY_PORT_J246           GPIOE
#define KEY_PIN_J246            GPIO_PIN_6
#define KEY_CTL_LINE_ENABLE     GPIO_PIN_RESET
#define KEY_CTL_LINE_DISABLE    GPIO_PIN_SET
#endif

#define RCC_KEY_BOARD_CLK_ENABLE() do { \
    __HAL_RCC_GPIOA_CLK_ENABLE();       \
    __HAL_RCC_GPIOC_CLK_ENABLE();       \
}while(0)

enum key_id_e {
    KEY_UP,
    KEY_DOWN,

#if (USER_KEY_BOARD_MATRIX)
    KEY_LEFT,
    KEY_RIGHT
#endif
};

/*按键 GPIO 初始化*/
void GPIO_Key_Board_Init(void);

#endif/*__KEY_BOARD_SAMPLE_H*/
